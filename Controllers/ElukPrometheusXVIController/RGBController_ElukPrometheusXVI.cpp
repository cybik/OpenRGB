/*---------------------------------------------*\
|  RGBController_ElukPrometheusXVI.cpp          |
|                                               |
|  Generic RGB Interface for ElukPrometheusXVI  |
|  kernel drivers for Chroma peripherals        |
|                                               |
|  Adam Honse (CalcProgrammer1) 6/15/2019       |
|  Renaud Lepage (cybik)       11/25/2021       |
\*---------------------------------------------*/

#include "RGBController_ElukPrometheusXVI.h"
#include "ElukPrometheusXVIDevices.h"

#include <fstream>
#include <unistd.h>

#include "LogManager.h"

using namespace std::chrono_literals;
//void RGBController_ElukPrometheusXVI::RGBController_ConfigureDevice(int device, pxvi_device* dev_ptr);

void RGBController_ElukPrometheusXVI::DeviceUpdateLEDs()
{
    DeviceUpdateMode();
}

void RGBController_ElukPrometheusXVI::UpdateZoneLEDs(int /*zone*/)
{
    DeviceUpdateLEDs();
}

void RGBController_ElukPrometheusXVI::UpdateSingleLED(int /*led*/)
{
    DeviceUpdateLEDs();
}

#define OPEN_DEVICE(X) (X.open(dev_path + "/parameters/" + #X))

void RGBController_ElukPrometheusXVI::OpenFunctions(std::string dev_path)
{
    base_path = dev_path + "/parameters/";
}

void RGBController_ElukPrometheusXVI::SetupDevice(std::string dev_path, e_eluk_device target)
{
    /*-----------------------------------------------------------------*\
    | Open the ElukPrometheusXVI device functions                       |
    \*-----------------------------------------------------------------*/
    OpenFunctions(dev_path);

    /*-----------------------------------------------------------------*\
    | Start device at -1.  This indicates the device was not detected   |
    \*-----------------------------------------------------------------*/
    device_index = -1;

    /*-----------------------------------------------------------------*\
    | Get the device name from the ElukPrometheusXVI driver             |
    \*-----------------------------------------------------------------*/
    //std::getline(device_type, name);
    // THIS DOES NOT EXIST. Do equivalent?
    //name = "Eluktronics Prometheus XVI";

    /*-----------------------------------------------------------------*\
    | Set the description to indicate this is a PrometheusXVI laptop    |
    \*-----------------------------------------------------------------*/
    description = "Eluktronics Prometheus XVI Platform";

    /*-----------------------------------------------------------------*\
    | Get the serial number from the dev path                           |
    \*-----------------------------------------------------------------*/
    //std::getline(device_serial, serial);

    /*-----------------------------------------------------------------*\
    | Get the firmware version from the dev path                        |
    \*-----------------------------------------------------------------*/
    //std::getline(firmware_version, version);

    /*-----------------------------------------------------------------*\
    | Vendor is always Eluktronics                                      |
    \*-----------------------------------------------------------------*/
    vendor = "Eluktronics";

    /*-----------------------------------------------------------------*\
    | Start with one mode for everyone                                  |
    \*-----------------------------------------------------------------*/
    mode Offline;
    Offline.name                  = "Offline";
    Offline.value                 = ELUK_PROMXVI_MODE_OFF;
    Offline.flags                 = MODE_FLAG_MANUAL_SAVE | MODE_FLAG_HAS_PER_LED_COLOR;
    Offline.color_mode            = MODE_COLORS_PER_LED; // not caring honestly
    Offline.colors_min            = 1;
    Offline.colors_max            = 1;
    Offline.brightness_max        = 0;
    Offline.brightness_min        = 0;
    Offline.brightness            = 0;
    Offline.colors.resize(1); // allow for one color to set the baseline
    modes.push_back(Offline);

    mode Solid;
    Solid.name                    = "Solid";
    Solid.value                   = ELUK_PROMXVI_MODE_STATIC;
    Solid.flags                   = MODE_FLAG_MANUAL_SAVE | MODE_FLAG_HAS_PER_LED_COLOR | MODE_FLAG_HAS_BRIGHTNESS;
    Solid.color_mode              = MODE_COLORS_PER_LED; // set colors
    Solid.colors_min              = 1;
    Solid.colors_max              = 1;
    Solid.brightness_max          = 2;
    Solid.brightness_min          = 0;
    Solid.brightness              = 0;
    Solid.colors.resize(1);
    modes.push_back(Solid);

    mode Breathing;
    Breathing.name                = "Breathing";
    Breathing.value               = ELUK_PROMXVI_MODE_BREATHING;
    Breathing.flags               = MODE_FLAG_MANUAL_SAVE | MODE_FLAG_HAS_PER_LED_COLOR | MODE_FLAG_HAS_BRIGHTNESS;
    Breathing.color_mode          = MODE_COLORS_PER_LED; // set colors
    Breathing.colors_min          = 1;
    Breathing.colors_max          = 1;
    Breathing.brightness_max      = 2;
    Breathing.brightness_min      = 0;
    Breathing.brightness          = 0;
    Breathing.colors.resize(1);
    modes.push_back(Breathing);

    mode Rainbow_Road;
    Rainbow_Road.name             = "Rainbow Progression";
    Rainbow_Road.value            = ELUK_PROMXVI_MODE_RAINBOWROAD;
    Rainbow_Road.flags            = MODE_FLAG_MANUAL_SAVE | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_BRIGHTNESS;
    Rainbow_Road.color_mode       = MODE_COLORS_MODE_SPECIFIC; // not exactly sure this would have colors
    Rainbow_Road.colors_min       = 1;
    Rainbow_Road.colors_max       = 1;
    Rainbow_Road.brightness_max   = 2;
    Rainbow_Road.brightness_min   = 0;
    Rainbow_Road.brightness       = 0;
    Rainbow_Road.colors.resize(1);
    modes.push_back(Rainbow_Road);
    if(target == ELUK_PROMXVI_DEVICE_KEYBOARD)
    {
        mode Ambient; // lock out of trunk and Logo pls
        Ambient.name                  = "Ambient";
        Ambient.value                 = ELUK_PROMXVI_MODE_AMBIENT;
        Ambient.flags                 = MODE_FLAG_MANUAL_SAVE | MODE_FLAG_HAS_MODE_SPECIFIC_COLOR | MODE_FLAG_HAS_BRIGHTNESS;
        Ambient.color_mode            = MODE_COLORS_MODE_SPECIFIC; // review on windows pls
        Ambient.colors_min            = 1;
        Ambient.colors_max            = 1;
        Ambient.brightness_max        = 2;
        Ambient.brightness_min        = 0;
        Ambient.brightness            = 0;
        Ambient.colors.resize(1);
        modes.push_back(Ambient);
    }
}

void RGBController_ElukPrometheusXVI::RGBController_ConfigureDevice(int device, const pxvi_device* dev_ptr)
{
    /*---------------------------------------------------------*\
    | Set device ID                                             |
    \*---------------------------------------------------------*/
    device_index = device;

    /*---------------------------------------------------------*\
    | Set device type                                           |
    \*---------------------------------------------------------*/
    type = dev_ptr->type;

    name = dev_ptr->name;
    SetupZones();
}

RGBController_ElukPrometheusXVI::RGBController_ElukPrometheusXVI(std::string dev_path, e_eluk_device target)
{
    /*---------------------------------------------------------*\
    | Internal Device Type Hint                                 |
    \*---------------------------------------------------------*/
    device_type_internal = target;

    SetupDevice(dev_path, target);
    RGBController_ConfigureDevice(target, device_list_split[target]);
}

RGBController_ElukPrometheusXVI::~RGBController_ElukPrometheusXVI()
{
    /*---------------------------------------------------------*\
    | Delete the matrix map                                     |
    \*---------------------------------------------------------*/
    for(unsigned int zone_index = 0; zone_index < zones.size(); zone_index++)
    {
        if(zones[zone_index].matrix_map != NULL)
        {
            if(zones[zone_index].matrix_map->map != NULL)
            {
                delete zones[zone_index].matrix_map->map;
            }

            delete zones[zone_index].matrix_map;
        }
    }
}

void RGBController_ElukPrometheusXVI::SetupZones(const pxvi_device* dev_ptr)
{
    for(int ctr = 0; ctr < dev_ptr->zone_count; ctr++) {
        zone new_zone;

        new_zone.name       = dev_ptr->zones[ctr]->name;
        new_zone.type       = dev_ptr->zones[ctr]->hint; //dev_ptr->zones[ctr]->type;

        new_zone.leds_count = 1;
        new_zone.leds_min   = new_zone.leds_count;
        new_zone.leds_max   = new_zone.leds_count;
        new_zone.matrix_map = nullptr;

        zones.push_back(new_zone);
        led local_led;
        local_led.name = new_zone.name;
        leds.push_back(local_led);
    }
}

void RGBController_ElukPrometheusXVI::SetupZones()
{
    /*---------------------------------------------------------*\
    | Fill in zone information based on device table            |
    \*---------------------------------------------------------*/
    SetupZones(device_list_split[device_type_internal]);
    SetupColors();
}

void RGBController_ElukPrometheusXVI::ResizeZone(int /*zone*/, int /*new_size*/)
{
    /*---------------------------------------------------------*\
    | This device does not support resizing zones               |
    \*---------------------------------------------------------*/
}

/*
 * READ op is special. The read is actually fake and just used to trigger a commit.
 */

#define OPEN_DEVICE_(X) (X.open(base_path + #X))
#define WRITE_DEVICE(X, Y) X.write(Y.c_str(), Y.size()); X.flush();
#define READ_DEVICE(X) X.peek();
#define CLOSE_DEVICE(X) if(X.is_open()){X.close();}

#define WRITE_OP(DEV, STRING) \
    OPEN_DEVICE_(DEV); \
    WRITE_DEVICE(DEV, STRING); \
    CLOSE_DEVICE(DEV);

#define READ_OP(DEV) \
    OPEN_DEVICE_(DEV); \
    READ_DEVICE(DEV); \
    CLOSE_DEVICE(DEV);

#define WRITE_COLOR(DEV, COLOR) \
    std::string _res = std::to_string(COLOR); \
    WRITE_OP(DEV, _res);

#define WRITE_EFFECT(DEV, EFFECT) \
    std::string _res_effect = std::to_string(EFFECT); \
    WRITE_OP(DEV, _res_effect);

#define WRITE_LEVEL(DEV, LEVEL) \
    std::string _res_level = std::to_string(LEVEL); \
    WRITE_OP(DEV, _res_level);

#define COMMIT_FAKE(DEV) \
    READ_OP(rgb_pretend_##DEV);

#define COMMIT(DEV) \
    READ_OP(rgb_##DEV);

void RGBController_ElukPrometheusXVI::set_trunk(uint effect, uint level, uint color)
{
    WRITE_EFFECT(rgb_trunk_effect, effect);
    WRITE_LEVEL(rgb_trunk_level, level);
    WRITE_COLOR(rgb_trunk_color, color);
}

#if defined(ELUK_EXP_LOGO_SUPPORT)
void RGBController_ElukPrometheusXVI::set_logo(uint effect, uint level, uint color)
{
    WRITE_EFFECT(rgb_logo_effect, effect);
    WRITE_LEVEL(rgb_logo_level, level);
    WRITE_COLOR(rgb_logo_color, color);
}
#endif
void RGBController_ElukPrometheusXVI::set_left(uint effect, uint level, uint color)
{
    WRITE_EFFECT(rgb_left_effect, effect);
    WRITE_LEVEL(rgb_left_level, level);
    WRITE_COLOR(rgb_left_color, color);
}
void RGBController_ElukPrometheusXVI::set_centre(uint effect, uint level, uint color)
{
    WRITE_EFFECT(rgb_cntr_effect, effect);
    WRITE_LEVEL(rgb_cntr_level, level);
    WRITE_COLOR(rgb_cntr_color, color);
}
void RGBController_ElukPrometheusXVI::set_right(uint effect, uint level, uint color)
{
    WRITE_EFFECT(rgb_right_effect, effect);
    WRITE_LEVEL(rgb_right_level, level);
    WRITE_COLOR(rgb_right_color, color);
}

void RGBController_ElukPrometheusXVI::commit_to_kbd()
{
    COMMIT(commit_kbd);
}
void RGBController_ElukPrometheusXVI::commit_to_trunk()
{
    COMMIT(commit_trunk);
}
#if defined(ELUK_EXP_LOGO_SUPPORT)
void RGBController_ElukPrometheusXVI::commit_to_logo()
{
    COMMIT_FAKE(commit_logo);
}
#endif

#define TO_RGB(rgb) ToRGBColor(RGBGetRValue(rgb),RGBGetGValue(rgb),RGBGetBValue(rgb))
#define TO_BGR(rgb) ToRGBColor(RGBGetBValue(rgb),RGBGetGValue(rgb),RGBGetRValue(rgb))

void RGBController_ElukPrometheusXVI::DeviceSaveMode() {
    switch (device_type_internal)
    {
        case(ELUK_PROMXVI_DEVICE_KEYBOARD):
        {
            for(auto _zone: this->zones)
            {
                switch((e_eluk_zone_hint)(_zone.type))
                {
                    case(ELUK_PROMXVI_DEVICE_HINT_KEYBOARD_LEFT):
                    {
                        set_left(modes[active_mode].value,
                                 modes[active_mode].brightness,
                                 TO_BGR(_zone.colors[0])
                        );
                        continue;
                    }
                    case(ELUK_PROMXVI_DEVICE_HINT_KEYBOARD_CENTRE):
                    {
                        set_centre(modes[active_mode].value,
                                   modes[active_mode].brightness,
                                   TO_BGR(_zone.colors[0])
                        );
                        continue;
                    }
                    case(ELUK_PROMXVI_DEVICE_HINT_KEYBOARD_RIGHT):
                    {
                        set_right(modes[active_mode].value,
                                  modes[active_mode].brightness,
                                  TO_BGR(_zone.colors[0])
                        );
                        continue;
                    }
                    default:
                    {
                        // wut
                        continue;
                    }
                }
            }
            commit_to_kbd();
            return;
        }
        case(ELUK_PROMXVI_DEVICE_TRUNK):
        {
            if(modes[active_mode].value == ELUK_PROMXVI_MODE_AMBIENT) return; // FIXME: unsupported on trunk, deactivate UI
            set_trunk(modes[active_mode].value,
                      modes[active_mode].brightness,
                      TO_BGR(this->zones[0].colors[0])
            );
            commit_to_trunk();
            return;
        }
#if defined(ELUK_EXP_LOGO_SUPPORT)
        case(ELUK_PROMXVI_DEVICE_LOGO):
        {
            if(modes[active_mode].value == ELUK_PROMXVI_MODE_AMBIENT)     return; // FIXME: unsupported on logo, deactivate UI
            if(modes[active_mode].value == ELUK_PROMXVI_MODE_RAINBOWROAD) return; // FIXME: unsupported on logo, deactivate UI
            set_logo(modes[active_mode].value,
                      modes[active_mode].brightness,
                      TO_BGR(this->zones[0].colors[0])
            );
            commit_to_logo();
            return;
        }
#endif
    }
}
