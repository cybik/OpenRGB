/*-----------------------------------------*\
|  RGBController_ElukPrometheusXVI.h                |
|                                           |
|  Generic RGB Interface for ElukPrometheusXVI      |
|  kernel drivers for Chroma peripherals    |
|                                           |
|  Adam Honse (CalcProgrammer1) 6/15/2019   |
|  Renaud Lepage (cybik)       11/25/2021   |
\*-----------------------------------------*/

#pragma once

#include "RGBController.h"
#include "ElukPrometheusXVIDevicesCommon.h"

#include <fstream>

#define ELUK_PXVI_TESTING_MULTI_DEVICE 1

class RGBController_ElukPrometheusXVI : public RGBController
{
public:
    enum
    {
        ELUK_PROMXVI_MODE_OFF         = 0,
        ELUK_PROMXVI_MODE_STATIC      = 1,
        ELUK_PROMXVI_MODE_BREATHING   = 3,
        ELUK_PROMXVI_MODE_RAINBOWROAD = 6,
        ELUK_PROMXVI_MODE_AMBIENT     = 7,
        ELUK_PROMXVI_NUM_MODES // wut
    };

    enum // no?
    {
        ELUK_PROMXVI_TYPE_EXISTS,
        ELUK_PROMXVI_NUM_TYPES
    };
    // ?

public:
    RGBController_ElukPrometheusXVI(std::string dev_path, e_eluk_device target);
    ~RGBController_ElukPrometheusXVI();

    void        SetupZones();

    void        ResizeZone(int zone, int new_size);

    void        DeviceUpdateLEDs();
    void        UpdateZoneLEDs(int zone);
    void        UpdateSingleLED(int led);

    void        SetCustomMode() {}
    void        DeviceUpdateMode() {}
    void        DeviceSaveMode();

    int         device_index;

private:
    e_eluk_device device_type_internal;
    void RGBController_ConfigureDevice(int device, const pxvi_device* dev_ptr);
    void SetupDevice(std::string dev_path, e_eluk_device target);
    void SetupZones(const pxvi_device* dev_ptr); // might need to restore to public? dunno

    void OpenFunctions(std::string dev_path);

    void set_trunk(uint effect, uint level, RGBColor color);
    void set_left(uint effect, uint level, RGBColor color);
    void set_centre(uint effect, uint level, RGBColor color);
    void set_right(uint effect, uint level, RGBColor color);
    void commit_to_kbd();
    void commit_to_trunk();

    /*
     * Centre ops
     */
    std::ofstream rgb_cntr_color;
    std::ofstream rgb_cntr_effect;
    std::ofstream rgb_cntr_level;

    /*
     * Left-side ops
     */
    std::ofstream rgb_left_color;
    std::ofstream rgb_left_effect;
    std::ofstream rgb_left_level;

    /*
     * Right-side ops
     */
    std::ofstream rgb_right_color;
    std::ofstream rgb_right_effect;
    std::ofstream rgb_right_level;

    /*
     * Trunk ops
     */
    std::ofstream rgb_trunk_color;
    std::ofstream rgb_trunk_effect;
    std::ofstream rgb_trunk_level;

    /*
     * Commit ops
     */
    std::ifstream rgb_commit_all;
    std::ifstream rgb_commit_kbd;
    std::ifstream rgb_commit_trunk;

    std::ifstream rgb_pretend_commit_all;
    std::ifstream rgb_pretend_commit_kbd;
    std::ifstream rgb_pretend_commit_trunk;

    std::string base_path;

#if defined(ELUK_EXP_LOGO_SUPPORT)
    void set_logo(uint effect, uint level, RGBColor color);
    void commit_to_logo();

    /*
     * Logo ops
     */
    std::ofstream rgb_logo_color;
    std::ofstream rgb_logo_effect;
    std::ofstream rgb_logo_level;

    std::ifstream rgb_commit_logo;
    std::ifstream rgb_pretend_commit_logo;
#endif

};
