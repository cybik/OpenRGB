#pragma once

#include <string>
#include <vector>
#include "RGBController.h"
#include "ElukPrometheusXVIDevicesCommon.h"

/*-------------------------------------------------------------------------*\
|  KEYBOARDS                                                                |
\*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------*\
|  Eluktronics Prometheus XVI                                   |
|                                                               |
|  Zone "Logo"                                                  |
|       Single                                                  |
|       1 LED                                                   |
|                                                               |
|  Zone "Trunk"                                                 |
|       Single                                                  |
|       1 LED                                                   |
|                                                               |
|  Zone "Keyboard Left"                                         |
|       Single                                                  |
|       1 LED                                                   |
|                                                               |
|  Zone "Keyboard Centre"                                       |
|       Single                                                  |
|       1 LED                                                   |
|                                                               |
|  Zone "Keyboard Right"                                        |
|       Single                                                  |
|       1 LED                                                   |
\*-------------------------------------------------------------*/
#if defined(ELUK_EXP_LOGO_SUPPORT)
static const pxvi_zone pxvi_led_logo_zone =
{
    "Logo",
    ZONE_TYPE_SINGLE,
    ELUK_PROMXVI_DEVICE_HINT_LOGO
};
#endif

static const pxvi_zone pxvi_led_trunk_zone =
{
    "Trunk",
    ZONE_TYPE_SINGLE,
    ELUK_PROMXVI_DEVICE_HINT_TRUNK
};

static const pxvi_zone pxvi_led_keyboard_left_zone =
{
    "Keyboard Left Side",
    ZONE_TYPE_SINGLE,
    ELUK_PROMXVI_DEVICE_HINT_KEYBOARD_LEFT
};

static const pxvi_zone pxvi_led_keyboard_centre_zone =
{
    "Keyboard Centre Side",
    ZONE_TYPE_SINGLE,
    ELUK_PROMXVI_DEVICE_HINT_KEYBOARD_CENTRE
};


static const pxvi_zone pxvi_led_keyboard_right_zone =
{
    "Keyboard Right Side",
    ZONE_TYPE_SINGLE,
    ELUK_PROMXVI_DEVICE_HINT_KEYBOARD_RIGHT
};


static const pxvi_device pxvi_keyboard =
{
    "Prometheus XVI Keyboard",
    DEVICE_TYPE_KEYBOARD,
    3,
    {
        &pxvi_led_keyboard_left_zone,
        &pxvi_led_keyboard_centre_zone,
        &pxvi_led_keyboard_right_zone,
        NULL
    }
};

static const pxvi_device pxvi_trunk =
{
    "Prometheus XVI Trunk",
    DEVICE_TYPE_COOLER,
    1,
    {
        &pxvi_led_trunk_zone,
        NULL,
        NULL,
        NULL
    }
};

#if defined(ELUK_EXP_LOGO_SUPPORT)
static const pxvi_device pxvi_logo =
{
    "Prometheus XVI Logo",
    DEVICE_TYPE_LIGHT,
    1,
    {
        &pxvi_led_logo_zone,
        NULL,
        NULL,
        NULL,
        NULL
    }
};
#endif

static const pxvi_device pxvi_laptop =
{
    "Prometheus XVI Laptop",
    DEVICE_TYPE_LIGHT,
	ELUK_PXVI_MAX_ZONES,
    {
#if defined(ELUK_EXP_LOGO_SUPPORT)
        &pxvi_led_logo_zone,
#endif
        &pxvi_led_trunk_zone,
        &pxvi_led_keyboard_left_zone,
        &pxvi_led_keyboard_centre_zone,
        &pxvi_led_keyboard_right_zone
    }
};

#if 0
static const pxvi_device* device_list_unified[] =
{
    &pxvi_laptop,
};
#endif

static const pxvi_device* device_list_split[] =
{
/*-----------------------------------------------------------------*\
|  KEYBOARDS                                                        |
\*-----------------------------------------------------------------*/
    &pxvi_keyboard,
/*-----------------------------------------------------------------*\
|  TRUNK                                                            |
\*-----------------------------------------------------------------*/
    &pxvi_trunk,

#if defined(ELUK_EXP_LOGO_SUPPORT)
/*-----------------------------------------------------------------*\
|  LOGO                                                             |
\*-----------------------------------------------------------------*/
    &pxvi_logo,
#endif
};

/*-------------------------------------------------------------------------*\
|  DEVICE MASTER LIST                                                       |
\*-------------------------------------------------------------------------*/
#define ELUK_NUM_DEVICES_UNI (sizeof(device_list_unified) / sizeof(device_list_unified[ 0 ]))
#define ELUK_NUM_DEVICES_SPL (sizeof(device_list_split)   / sizeof(device_list_split  [ 0 ]))

