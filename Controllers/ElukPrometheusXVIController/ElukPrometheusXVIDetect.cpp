#include "Detector.h"
#include "LogManager.h"
#include "ElukPrometheusXVIDetect.h"
#include <vector>
#include <stdio.h>
#include <stdlib.h>

#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>

/******************************************************************************************\
*                                                                                          *
*   DetectElukPrometheusXVIControllers                                                     *
*                                                                                          *
*       Detect devices supported by the eluk-led-wmi kernel drivers                        *
*                                                                                          *
\******************************************************************************************/

void AddPrometheusXVIController(std::vector<RGBController*> &rgb_controllers, const char* device_string, e_eluk_device device)
{
    RGBController_ElukPrometheusXVI * prom_xvi_rgb = new RGBController_ElukPrometheusXVI(device_string, device);

    if(prom_xvi_rgb->device_index != -1)
    {
        rgb_controllers.push_back(prom_xvi_rgb);
    }
    else
    {
        LOG_DEBUG("[ElukPrometheusXVI] Device index is not -1, delete new controller");
        delete prom_xvi_rgb;
    }

}
void DetectElukPrometheusXVIControllers(std::vector<RGBController*> &rgb_controllers)
{
    char driver_path[512];
    DIR *dir;
    struct dirent *ent;
    bool done = false;

    strcpy(driver_path, "/sys/bus/wmi/drivers/eluk-pxvi-led-wmi/");

    done = false;

    dir = opendir(driver_path);

    LOG_INFO("[ElukPrometheusXVI] folder check %s is %s", driver_path, (dir == NULL)?"not found":"found driver." );

    if(dir == NULL)
    {
        return;
    }

    ent = readdir(dir);

    while(ent != NULL)
    {
        if(ent->d_type == DT_DIR || ent->d_type == DT_LNK)
        {
            if(!strcmp(ent->d_name, "."))
            {
                if(done == false)
                {
                    done = true;
                }
                else
                {
                    break;
                }
            }
            else if(!strcmp(ent->d_name, ".."))
            {
            }
            else if(!strcmp(ent->d_name, "module")) // ! is false is 0 is match. don't ask.
            {
                char device_string[1024];
                uint16_t size = sizeof(driver_path);
                strncpy(device_string, driver_path, size);
                size = sizeof(ent->d_name);
                strncat(device_string, ent->d_name, size);

                AddPrometheusXVIController(rgb_controllers, device_string, e_eluk_device::ELUK_PROMXVI_DEVICE_KEYBOARD);
                AddPrometheusXVIController(rgb_controllers, device_string, e_eluk_device::ELUK_PROMXVI_DEVICE_TRUNK);
#if defined(ELUK_EXP_LOGO_SUPPORT)
                AddPrometheusXVIController(rgb_controllers, device_string, e_eluk_device::ELUK_PROMXVI_DEVICE_LOGO);
#endif
            }
        }

        ent = readdir(dir);
    }

    closedir(dir);

}   /* DetectElukPrometheusXVIControllers() */

REGISTER_DETECTOR("Eluktronics Prometheus XVI", DetectElukPrometheusXVIControllers);
