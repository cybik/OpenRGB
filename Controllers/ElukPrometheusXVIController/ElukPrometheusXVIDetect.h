#ifndef ELUKPROMETHEUSXVIDETECT_H
#define ELUKPROMETHEUSXVIDETECT_H

#include "RGBController.h"
#include "RGBController_ElukPrometheusXVI.h"
void AddPrometheusXVIController(std::vector<RGBController*> &rgb_controllers, const char* device_string, e_eluk_device device);


#endif // ELUKPROMETHEUSXVIDETECT_H
