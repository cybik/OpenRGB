#ifndef ELUKPROMETHEUSXVIDEVICESCOMMON_H
#define ELUKPROMETHEUSXVIDEVICESCOMMON_H

#if defined(ELUK_EXP_LOGO_SUPPORT)
#define ELUK_PXVI_MAX_ZONES 5
#else
#define ELUK_PXVI_MAX_ZONES 4
#define ELUK_PXVI_DEV_ZONES 3
#endif

#include "RGBController.h"
#include <string>

typedef enum {
    ELUK_PROMXVI_DEVICE_HINT_KEYBOARD_LEFT,
    ELUK_PROMXVI_DEVICE_HINT_KEYBOARD_CENTRE,
    ELUK_PROMXVI_DEVICE_HINT_KEYBOARD_RIGHT,
    ELUK_PROMXVI_DEVICE_HINT_TRUNK,
#if defined(ELUK_EXP_LOGO_SUPPORT)
    ELUK_PROMXVI_DEVICE_HINT_LOGO
#endif
} e_eluk_zone_hint;

typedef struct
{
    std::string name;
    unsigned int type;
    e_eluk_zone_hint hint;
} pxvi_zone;

typedef struct
{
    std::string name;
    device_type type;
    int zone_count;
    const pxvi_zone* zones[ELUK_PXVI_MAX_ZONES];
} pxvi_device;

typedef enum {
    ELUK_PROMXVI_DEVICE_KEYBOARD,
    ELUK_PROMXVI_DEVICE_TRUNK,
#if defined(ELUK_EXP_LOGO_SUPPORT)
    ELUK_PROMXVI_DEVICE_LOGO
#endif
} e_eluk_device;

#endif // ELUKPROMETHEUSXVIDEVICESCOMMON_H
